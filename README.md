# `sre` & `rxxc`: regex-replace in files / symlink targets

`sre` is a command-line tool to search for regex patterns and replace them with fixed strings or computed expressions. It can operate on files, or in symlink targets; it can modify the inputs, or print to standard output; it can perform any multi-file search & replace operation that would normally require an advanced editor.

Bonus: [`rxxc`](bin/rxxc) performs regex substitution in filenames, just like the venerable `[p]rename` / [`File::Rename`](https://metacpan.org/pod/distribution/File-Rename/rename.PL), but [allows arbitrary commands](#rxxc) ('`git mv`') or even inline bash code. It can also generate a script for further inspection / processing (`--dry-run`).

Some further documentation is in the GitLab [wiki](https://gitlab.com/kstr0k/sre/-/wikis/home), mirrored here in branch [wiki](https://gitlab.com/kstr0k/sre/-/tree/wiki).

## `sre`

```
Arguments: [OPTION]... FIND REPLACE [FILE]...
Options:
--help
--delim [STR] : instead of newline
--eval | -e : eval() REPLACEMENT (otherwise a literal string)
--no-eval
--fixed-strings | -F : FIND is not a regex (like grep -F)
--no-fixed-strings
--inplace [STR] : modify input files, STR = suffix for backups
--ln | -L : substitute in symlink targets, not in files
--no-ln
--zero | -z : lines are NUL (\0)-terminated

'--eval' turns on perl's /ee modifier, so $1, $2 must be quoted.
# sre '[0-9]' 'x'
# sre -e '([0-9])' '" digit:$1 "'       # note the inner ""!
# sre -e '(?<dig>[0-9])' '2 * $+{dig}'  # named capture + expression
# basename -a -z /usr/bin/* | sre -z - ^  | tr '\0' '\n' | less
```

## [`rxxc`](https://gitlab.com/kstr0k/sre/-/wikis/Tools/rxxc)

```
Usage: $0 [-x CMD] [-n|--dry-run] [-v|--verbose] [--OPT].. PERLEXPR [FILE]..

CMD='mv -iT' by default. Other options:
--unchanged  : disable default no-rename check
--no-clobber : skip if destination exists (-i in default CMD prompts anyway)

CMD is a command taking FROM TO args, or bash code that uses "$1", "$2":
  $0 -x 'git mv' 's/old/new/' *.md
  $0 -x '[[ ! -e $2 || $1 -nt $2 ]] && cp -iT "$1" "$2"' 's/$/.bak/' *
--dry-run output can be executed (e.g. eval or '| bash'), edited etc
```

## Installing

Clone this git repo. [`bin/sre`](bin/sre) depends on [`Getopt::Auto::Long::Usage`](https://metacpan.org/pod/Getopt::Auto::Long::Usage); either install it (e.g. via `cpanm`), or run the `setup` script and use the generated `bin/sre.standalone`. There's also [`bin/sre.fromrepo`](bin/sre.fromrepo), which auto-invokes `setup` if needed, and sets PERL5LIB appropriately before passing the buck to `sre`.

[`bin/rxxc`](bin/rxxc) is a standalone Perl program with no dependencies.

## Copyright

[`Alin Mr. <almr.oss@outlook.com>` / MIT license](LICENSE.txt)
